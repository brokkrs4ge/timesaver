package datatype_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/brokkrs4ge/timesaver/datatype"
)

func Test_Int(t *testing.T) {
	var intVal int = 5

	ptr := datatype.Ptr(intVal)

	asserter := assert.New(t)
	asserter.Equal(
		intVal,
		datatype.Val[int](ptr),
	)
}

func TestCustomType(t *testing.T) {
	type human struct {
		Name string
	}

	var h = human{
		"ALI",
	}
	ptr := datatype.Ptr(h)
	assert.Equal(t, h, datatype.Val[human](ptr))
}
