package datatype

// Ptr takes in value of type T and returns pointer  T
func Ptr[T any](input T) *T {
	return &input
}

// Val takes in pointer of type T and returns value  T
func Val[T any](input *T) T {
	return *input
}
